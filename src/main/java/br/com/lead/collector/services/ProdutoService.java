package br.com.lead.collector.services;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    ProdutoRepository produtoRepository;

    public Produto salvarProduto(Produto produto){
        if(produtoRepository.existsById(produto.getId())){
            throw new RuntimeException("O ID não deve ser informado ao criar um novo produto.");
        }
        return produtoRepository.save(produto);
    }

    public Iterable<Produto> consultarTodosProdutos(){
        return produtoRepository.findAll();
    }

    public Optional<Produto> consultarPorId(int id){
        if(produtoRepository.existsById(id)){
            return produtoRepository.findById(id);
        }
        throw new RuntimeException("Produto não existe. ID informado: " + id);
    }

    public Produto atualizarProduto(Produto produto, int id){
        Produto produtoDB = consultarPorId(id).get();
        produto.setId(produtoDB.getId());
        return produtoRepository.save(produto);
    }

    public void deletar(int id){
        if(produtoRepository.existsById(id)){
            produtoRepository.deleteById(id);
        } else {
            throw new RuntimeException("Produto não existe. ID informado: " + id);
        }
    }

}
