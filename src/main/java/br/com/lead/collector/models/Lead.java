package br.com.lead.collector.models;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "lead")
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nome;
    private String cpf;
    private String email;
    private String telefone;
    private LocalDate dataDeCadastro;

    @ManyToMany
    private List<Produto> produtos;

    public Lead() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

    public LocalDate getDataDeCadastro() {
        return dataDeCadastro;
    }

    public void setDataDeCadastro(LocalDate dataDeCadastro) {
        this.dataDeCadastro = dataDeCadastro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lead lead = (Lead) o;
        return id == lead.id &&
                Objects.equals(nome, lead.nome) &&
                Objects.equals(cpf, lead.cpf) &&
                Objects.equals(email, lead.email) &&
                Objects.equals(telefone, lead.telefone) &&
                Objects.equals(dataDeCadastro, lead.dataDeCadastro) &&
                Objects.equals(produtos, lead.produtos);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nome, cpf, email, telefone, dataDeCadastro, produtos);
    }

    @Override
    public String toString() {
        return "Lead{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", cpf='" + cpf + '\'' +
                ", email='" + email + '\'' +
                ", telefone='" + telefone + '\'' +
                ", dataDeCadastro=" + dataDeCadastro +
                ", produtos=" + produtos +
                '}';
    }
}
