package br.com.lead.collector.services;

import br.com.lead.collector.DTOs.CadastroDeLeadDTO;
import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTest {

    @MockBean
    private LeadRepository leadRepository;

    @MockBean
    private ProdutoRepository produtoRepository;

    @Autowired
    private LeadService leadService;

    Lead lead;
    Produto produto;
    CadastroDeLeadDTO cadastroDeLeadDTO;
    List<IdProdutoDTO> listaIdProdutoDTO;

    @BeforeEach
    private void setUp(){
        this.lead = new Lead();
        lead.setId(0);
        lead.setNome("Lucas");
        lead.setDataDeCadastro(LocalDate.now());
        lead.setTelefone("11 9 9999 5555");
        lead.setCpf("415.669.740-11");
        lead.setEmail("lucas@email.com");

        this.produto = new Produto();
        produto.setPreco(215.25);
        produto.setNome("Tinta Coral 18L");
        produto.setDescricao("Tinta cor Branco Gelo com acabamento fosco para ambientes internos.");
        produto.setId(1);

        lead.setProdutos(Arrays.asList(produto));

        listaIdProdutoDTO = new ArrayList<>();
        listaIdProdutoDTO.add(new IdProdutoDTO());

        this.cadastroDeLeadDTO = new CadastroDeLeadDTO();
        cadastroDeLeadDTO.setNome(lead.getNome());
        cadastroDeLeadDTO.setCpf(lead.getCpf());
        cadastroDeLeadDTO.setEmail(lead.getEmail());
        cadastroDeLeadDTO.setTelefone(lead.getTelefone());
        cadastroDeLeadDTO.setProdutos(listaIdProdutoDTO);
    }

    @Test
    public void testarBuscarLeadPeloId(){
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn((Optional.of(lead)));

        Assertions.assertEquals(lead, leadService.buscarLeadPeloId(8504));
    }

    @Test
    public void testarBuscaDeLeadPeloIdQueNaoExiste(){
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn((Optional.empty()));

        Assertions.assertThrows(RuntimeException.class, () -> {leadService.buscarLeadPeloId(234);});
    }

    @Test
    public void testarSalvarLead(){
        Mockito.when(produtoRepository.findAllById(Mockito.any())).thenReturn(Arrays.asList(this.produto));
        Mockito.when(leadRepository.save(Mockito.any())).then(object -> object.getArgument(0));

        Assertions.assertEquals(lead, leadService.salvarLead(cadastroDeLeadDTO));
    }
}
