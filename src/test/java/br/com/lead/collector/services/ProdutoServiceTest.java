package br.com.lead.collector.services;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class ProdutoServiceTest {

    @MockBean
    private ProdutoRepository produtoRepository;

    @Autowired
    private ProdutoService produtoService;

    Produto produto;
    List<Produto> listaProduto;

    @BeforeEach
    private void setUp(){
        produto = new Produto();
        produto.setId(1);
        produto.setNome("Plano de Internet");
        produto.setPreco(100.20);

        listaProduto = new ArrayList<>();
        listaProduto.add(produto);
    }

    @Test
    public void testarConsultarTodosProdutos(){
        Mockito.when(produtoRepository.findAll()).thenReturn(listaProduto);

        Assertions.assertEquals(listaProduto, produtoService.consultarTodosProdutos());
    }

    @Test
    public void testarConsultarPorId(){
        Mockito.when(produtoRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(produto));

        Assertions.assertEquals(produto, produtoService.consultarPorId(11241).get());
    }

    @Test
    public void testarConsultarPorIdQueNaoExiste(){
        Mockito.when(produtoRepository.existsById(Mockito.anyInt())).thenReturn(false);

        Assertions.assertThrows(RuntimeException.class, () -> {produtoService.consultarPorId(4432);});
    }

    @Test
    public void testarSalvarProduto(){
        Mockito.when(produtoRepository.existsById(Mockito.anyInt())).thenReturn(false);
        Mockito.when(produtoRepository.save(produto)).then(object -> object.getArgument(0));

        Assertions.assertEquals(produto, produtoService.salvarProduto(produto));
    }
}
