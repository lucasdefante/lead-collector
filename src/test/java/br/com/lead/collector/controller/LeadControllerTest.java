package br.com.lead.collector.controller;

import br.com.lead.collector.DTOs.CadastroDeLeadDTO;
import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.DTOs.ResumoDeLeadDTO;
import br.com.lead.collector.controllers.LeadController;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.services.LeadService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(LeadController.class)
public class LeadControllerTest {

    @MockBean
    private LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    Lead lead;
    List<ResumoDeLeadDTO> listaLeadDTO;
    CadastroDeLeadDTO cadastroDeLeadDTO;

    @BeforeEach
    private void setUp(){
        lead = new Lead();
        lead.setId(1);
        lead.setNome("Lucas D");

        listaLeadDTO = new ArrayList<>();
        for (int i=0; i<3; i++){
            ResumoDeLeadDTO resumoDeLeadDTO = new ResumoDeLeadDTO();
            resumoDeLeadDTO.setEmail(lead.getEmail());
            resumoDeLeadDTO.setNome(lead.getNome());
            resumoDeLeadDTO.setId(lead.getId());

            listaLeadDTO.add(resumoDeLeadDTO);
        }

        this.cadastroDeLeadDTO = new CadastroDeLeadDTO();
        cadastroDeLeadDTO.setNome("Lucas D");
        cadastroDeLeadDTO.setCpf("415.329.110-25");
        cadastroDeLeadDTO.setEmail("lucas@email.com");
        cadastroDeLeadDTO.setProdutos(new ArrayList<IdProdutoDTO>());
    }

    @Test
    public void testarBuscarPorId() throws Exception {
        Mockito.when(leadService.buscarLeadPeloId(Mockito.anyInt())).thenReturn(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarBuscarPorIdQueNaoExiste() throws Exception {
        Mockito.when(leadService.buscarLeadPeloId(1)).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarLerTodosOsLeads() throws Exception {
        Mockito.when(leadService.consultarTodosLeads()).thenReturn(listaLeadDTO);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads").
                contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray());
    }

    @Test
    public void testarCadastrarLead() throws Exception{
        Mockito.when(leadService.salvarLead(Mockito.any(CadastroDeLeadDTO.class))).thenReturn(lead);
        ObjectMapper objectMapper = new ObjectMapper();

        String leadJson = objectMapper.writeValueAsString(cadastroDeLeadDTO);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON).content(leadJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Lucas D")));
    }

    @Test
    public void testarValidacaoDeCadastroDeLead() throws Exception {
        //cadastroDeLeadDTO.setCpf("sdc5784");
        cadastroDeLeadDTO.setEmail("54545");
        ObjectMapper objectMapper = new ObjectMapper();
        String leadJson = objectMapper.writeValueAsString(cadastroDeLeadDTO);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON).content(leadJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

        Mockito.verify(leadService, Mockito.times(0))
                .salvarLead(Mockito.any(CadastroDeLeadDTO.class));
    }
}
